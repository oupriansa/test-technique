import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");

Vue.mixin({
  methods: {
    formatPrice(price) {
      if (isNaN(price)) {
        console.error("wrong price format", price);
        return 0;
      }
      return (
        new Intl.NumberFormat("en-US", {
          minimumFractionDigits: 0,
          maximumFractionDigits: 0,
        }).format(price) + " $"
      );
    },
  },
});
