import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: {},
    cart: [],
  },
  mutations: {
    SET_PRODUCTS(state, products) {
      state.products = products;
    },
    ADD_CART_ITEM(state, { product, quantity }) {
      state.cart = [...state.cart, { ...product, quantity }];
    },
    UPDATE_CART_ITEM(state, product) {
      const cartProductIndex = state.cart.findIndex((cartItem) => {
        return cartItem.id === product.id;
      });

      if (cartProductIndex < 0) {
        return false;
      }

      state.cart.splice(cartProductIndex, 1, product);
    },
    DELETE_CART_ITEM(state, product) {
      state.cart = state.cart.filter((cartItem) => {
        return cartItem.id !== product.id;
      });
    },
    CLEAR_CART(state) {
      state.cart = [];
    },
  },
  actions: {
    getProducts({ commit }) {
      const data = require("@/data/products.json");
      commit("SET_PRODUCTS", data.products);
    },
    addToCart({ commit, state }, { product, quantity }) {
      const currentProduct = state.cart.find(
        (cartItem) => cartItem.id === product.id
      );

      if (currentProduct) {
        commit("UPDATE_CART_ITEM", {
          ...currentProduct,
          quantity: currentProduct.quantity + quantity,
        });

        return;
      }

      commit("ADD_CART_ITEM", { product, quantity });
    },
  },
  getters: {
    totalPrice: (state) => {
      return state.cart.reduce((total, product) => {
        return total + product.price * product.quantity;
      }, 0);
    },
    featuredProduct: (state) => {
      return state.products["featured"];
    },
    justBookedProduct: (state) => {
      return state.products["just_booked"];
    },
  },
});
