# Test-technique

This is an e-commerce app create for Bird office.
it is a technical test.

Running ```with vue-cli```

Using : ```Vue, vue-router, vue-x, scss, babel, eslint and prettier```

Follow the step bellow to run the project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
